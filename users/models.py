import os
from django.db import models
from django.contrib.auth.models import User
from PIL import Image


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default= 'default.jpg', upload_to='profile_pics')
    address = models.CharField("Address",max_length=1024,default='')
    flatNumber = models.CharField("Flat Number",max_length=1024,default='')
    point = models.CharField( "Point",max_length=1024,default='')
    test = models.CharField(default='', max_length=1)

    # @property
    # def longitude(self):
    #     return self.point[0]
    #
    # @property
    # def latitude(self):
    #     return self.point[1]

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)