from django.shortcuts import render
from django.db.models import Q
from P2P_Renting.models import Post
from users.models import Profile
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin  # For classes
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

import math

# Create your views here.
class SearchProductListView(LoginRequiredMixin, ListView):
    model = Post
    template_name = 'search/view.html'  # Named as <app name>/<model using>_<viewtype>.html
    context_object_name = 'posts'
    paginate_by = 5
    # ordering = ['-date_posted'] ## Minus sign sorts the list from newest to oldest on the field we want to order on

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(SearchProductListView, self).get_context_data(**kwargs)
        query = self.request.GET.get('q')
        if query.strip() != "":
            context['query'] = query
        return context

    def get_queryset(self):
        request = self.request
        print("Request GET>  " + str(request.GET))
        query = request.GET.get('q')
        if query is not None and query.strip() != "":
            MyList = self.get_nearest_users() # Dict with user_id and distance
            lookups = Q(title__icontains=query) | Q(content__icontains=query) | Q(price__icontains=query)
            MyPost_filter1 = Post.objects.filter(lookups).distinct()
            MyPost_filter2 =  MyPost_filter1.filter(author_id__in=MyList)
            for post in MyPost_filter2:
                post.distance = MyList.get(post.author_id)
            # return Post.objects.filter(title__icontains=query)
            return MyPost_filter2   #Post.objects.filter(lookups).distinct()
        else:
            return Post.objects.none()


    def get_nearest_users(self):
        latLong1 = '0,0'
        latLong2 = '0,0'
        userList = {}

        for u in Profile.objects.raw('select id,user_id, point from Users_profile where user_id = ' + str(self.request.user.id) ):
            if(u.point != ''):
                latLong1 = u.point

        for p in Profile.objects.raw('select id,user_id, point from Users_profile'):
            if(p.user_id != self.request.user.id):
                if (p.point != ''):
                    latLong2 = p.point
                    if(self.calculate_distance(latLong1,latLong2) ):
                        for post in Post.objects.raw('select id from p2p_Renting_Post where author_id = '+ str(p.user_id)):
                            post.distance = self.calculate_distance(latLong1,latLong2)
                        userList[p.user_id] = post.distance
        return userList


    def calculate_distance( self,latLong1, latLong2):
        print(str(latLong1) + " ------ " + str(latLong2))
        lat1_list = latLong1.split(',')
        lat1 = float(lat1_list[0])
        long1= float(lat1_list[1])

        lat1_list = latLong2.split(',')
        lat2 = float(lat1_list[0])
        long2 = float(lat1_list[1])

        DegToRad = 57.29577951

        Ans = math.sin(lat1 / DegToRad)*math.sin(lat2 / DegToRad) + math.cos(lat1 / DegToRad) * math.cos(lat2 / DegToRad) * math.cos(
            math.fabs(long2 - long1) / DegToRad)
        Miles = 3959 * math.atan(math.sqrt(1 - math.pow(Ans, 2)) / Ans)
        Miles = round( Miles, 1)
        distanceinKM = Miles * 1.60934

        return distanceinKM