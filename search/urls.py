from django.conf.urls import url
from django.urls import path, include

from search.views import SearchProductListView

app_name = 'search'

urlpatterns = [
    path('', SearchProductListView.as_view(), name='search_view'),
    ## For Registration page
    # path('register/', user_views.register, name = "register"),

]