from django.apps import AppConfig


class P2PRentingConfig(AppConfig):
    name = 'P2P_Renting'
