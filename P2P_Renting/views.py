from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post
from cart.models import Cart
from users.models import Profile
import math
from .forms import ProductUpdateForm


def home(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'P2P_Renting/base.html', context)

class PostListView(LoginRequiredMixin, ListView):
    model = Post
    template_name = 'P2P_Renting/home.html'  # Named as <app name>/<model using>_<viewtype>.html
    context_object_name = 'posts'
    paginate_by = 5
    # ordering = ['-date_posted'] ## Minus sign sorts the list from newest to oldest on the field we want to order on

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self):

        MyList = PostListView.get_nearest_users(self)
        MyPost = Post.objects.filter(author_id__in=MyList);
        for post in MyPost:
            post.distance = MyList.get(post.author_id)
        return MyPost
        # return Post.objects.exclude(author_id = self.request.user.id).order_by('-date_posted')

    def get_nearest_users(self):
        latLong1 = '0,0'
        latLong2 = '0,0'
        userList = {}

        for u in Profile.objects.raw('select id,user_id, point from Users_profile where user_id = ' + str(self.request.user.id) ):
            if(u.point != ''):
                latLong1 = u.point

        for p in Profile.objects.raw('select id,user_id, point from Users_profile'):
            if(p.user_id != self.request.user.id):
                if (p.point != ''):
                    latLong2 = p.point
                    if(PostListView.calculate_distance(self,latLong1,latLong2) ):
                        for post in Post.objects.raw('select id from p2p_Renting_Post where author_id = '+ str(p.user_id)):
                            post.distance = PostListView.calculate_distance(self,latLong1,latLong2)
                        userList[p.user_id] = post.distance
        return userList

    def calculate_distance( self,latLong1, latLong2):
        # print(str(latLong1) + " ------ " + str(latLong2))
        lat1_list = latLong1.split(',')
        lat1 = float(lat1_list[0])
        long1= float(lat1_list[1])

        lat1_list = latLong2.split(',')
        lat2 = float(lat1_list[0])
        long2 = float(lat1_list[1])

        DegToRad = 57.29577951

        Ans = math.sin(lat1 / DegToRad)*math.sin(lat2 / DegToRad) + math.cos(lat1 / DegToRad) * math.cos(lat2 / DegToRad) * math.cos(
            math.fabs(long2 - long1) / DegToRad)
        Miles = 3959 * math.atan(math.sqrt(1 - math.pow(Ans, 2)) / Ans)
        Miles = round( Miles, 1)
        distanceinKM = Miles * 1.60934

        return distanceinKM



class PostDetailView(DetailView):
    model = Post
    template_name = 'P2P_Renting/post_detail.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

class PostCreateView(LoginRequiredMixin, CreateView):  ## Template to be named as <app name>/<model using>_form.html
    model = Post
    fields = ['title', 'content', 'price', 'product_image']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):  ## Template to be named as <app name>/<model using>_form.html
    # form_class = ProductUpdateForm
    model = Post
    fields = ['title', 'content', 'price', 'product_image']


    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):   ## So that no other User can update another Users Post ... will throw 403 forbidden
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView): ## Templates will expext to be in <model>_confirm_delete as this command tries to delete
    model = Post
    success_url = '/'

    def test_func(self):   ## So that no other User can delete another Users Post ... will throw 403 forbidden
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False

def about(request):
    return render(request, 'P2P_Renting/about.html', {'title': 'About'})

# def myPosts(request):
#     ordering = ['-date_posted']
#     context = {
#         'posts': Post.objects.all()
#     }
#     return render(request, 'P2P_Renting/myPost.html', context)

class myPost_PostListView(ListView):
    model = Post
    template_name = 'P2P_Renting/myPost.html'  # Named as <app name>/<model using>_<viewtype>.html
    context_object_name = 'posts'
    ordering = ['-date_posted']  # Minus sign sorts the list from newest to oldest on the field we want to order on
    paginate_by = 1

    def get_queryset(self):
        return Post.objects.filter(author_id = self.request.user.id).order_by('-date_posted')


# from django.shortcuts import render
#
# import logging
# import json
#
# from django.shortcuts import render
# from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
# from rest_framework.views import APIView
# from django.core.serializers.json import DjangoJSONEncoder
# from django.http import HttpResponse
#
# logger = logging.getLogger(__name__)
#
# # class Renting(APIView): ### Cant use POST as Chrome uses GET Method
# #     parser_classes = (MultiPartParser, FormParser, JSONParser,)
# #
# #     def post(self, request):
# #         logger.info('Info Retrieval')
# #         # input_data = request.dataP2PRentingConfig
# #
# #         return HttpResponse("HI")
# #         # return HttpResponse(json.dumps("HI", cls=DjangoJSONEncoder), content_type="application/json")
#
# posts = [
#     {
#         'author': 'CoreyMS',
#         'title': 'Blog Post 1',
#         'content': 'First post content',
#         'date_posted': 'August 27, 2018'
#     },
#     {
#         'author': 'Jane Doe',
#         'title': 'Blog Post 2',
#         'content': 'Second post content',
#         'date_posted': 'August 28, 2018'
#     },
#     {
#         'author': 'Suman Choudhuri',
#         'title': 'Blog Post 3',
#         'content': 'Third post content',
#         'date_posted': 'August 29, 2018'
#     },
# {
#         'author': 'Bikram K',
#         'title': 'Blog Post 4',
#         'content': 'Fourth post content',
#         'date_posted': 'August 2, 2018'
#     }
# ]
#
# def home(request):
#     logger.info('Info Retrieval')
#     # input_data = request.dataP2PRentingConfig
#     context = {
#         'posts': posts
#     }
#     return render(request, 'P2P_Renting/home.html', context)
#     # return HttpResponse('<h1>This is Home Page</h1>')
#
# def about(request):
#     # return HttpResponse('<h1>This is About Page</h1>')
#     return render(request, 'P2P_Renting/about.html')