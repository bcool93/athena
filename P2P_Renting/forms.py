from django import forms
from .models import Post


class ProductUpdateForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['title', 'content', 'price', 'product_image']

