from django.urls import path
from .views import PostListView, PostDetailView, PostCreateView, PostUpdateView, PostDeleteView, myPost_PostListView
from . import views

urlpatterns = [
    # path('', Renting.as_view(), name = "P2P_Renting-Home"),
    path('', PostListView.as_view(), name = "P2P_Renting-Home"),
    path('post/<int:pk>/', PostDetailView.as_view(), name = "P2P_Renting-Detail"), ## pk is primary key which is post id here
    path('post/new/', PostCreateView.as_view(), name = "P2P_Renting-Create"),
    path('post/<int:pk>/update/', PostUpdateView.as_view(), name = "P2P_Renting-Update"),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name = "P2P_Renting-Delete"),
    path('about/', views.about, name = "P2P_Renting-About"),
    # path('myPosts/', views.myPosts, name = "myPosts"),
    path('myPosts/', myPost_PostListView.as_view(), name = "myPosts"),
]