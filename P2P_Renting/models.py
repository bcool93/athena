import os
from PIL import Image
from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User

def upload_path_handler(instance, filename):
    user_path = os.path.join("product_pictures", instance.user.id)
    if not os.path.exists(user_path):
        os.makedirs(user_path)
    return os.path.join(user_path, filename)

class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    price = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)  ## price set by author
    is_rented = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    product_image = models.ImageField(default='product_default.jpg', upload_to='product_pictures') #upload_path_handler
    distance = 0
    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        print(self.product_image.path)

        img = Image.open(self.product_image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.product_image.path)

    def get_absolute_url(self):
        return reverse('P2P_Renting-Detail', kwargs={'pk': self.pk})
