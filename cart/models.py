from decimal import Decimal
from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save, post_save, m2m_changed
from P2P_Renting.models import Post
from users.models import Profile

User = settings.AUTH_USER_MODEL

# Create your models here.

class CartManager(models.Manager):
    def new_or_get(self, request):
        print(request)
        # cart_id = request.session.get("cart_id", None)
        user_instance = request.user
        print(user_instance)
        if request.user.is_authenticated:
            qs = self.get_queryset().filter(user=user_instance)
        else:
            cart_id = request.session.get("cart_id", None)
            qs = self.get_queryset().filter(id=cart_id)
        print("qs:  " + str(qs))
        if qs.count() == 1:
            new_obj = False
            cart_obj = qs.first()
            if request.user.is_authenticated and cart_obj.user is None:
                cart_obj.user = request.user
                cart_obj.save()
        elif qs.count() > 1:  ## Just to understand some exceptions
            print("Check!!  User has more than 1 cart created under his name: " + str(user_instance))
            new_obj = False
            cart_obj = None  ## ignore, will throw error
        else:
            cart_obj = Cart.objects.new(user = user_instance)
            new_obj = True
            request.session['cart_id'] = cart_obj.id
        return cart_obj, new_obj

    def new(self, user=None):
        user_obj = None
        if user is not None:
            if user.is_authenticated:
                user_obj = user
        return self.model.objects.create(user=user_obj)

class Cart(models.Model):
    user = models.ForeignKey(User, null = True, on_delete=models.SET_NULL)
    products = models.ManyToManyField(Post, blank = True)
    subtotal = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    total = models.DecimalField(default = 0.00, max_digits=100, decimal_places=2)
    updated = models.DateTimeField(auto_now=True, null = True) ## remove Null
    timestamp = models.DateTimeField(auto_now_add=True, null = True) ## remove Null

    objects = CartManager()

    def __str__(self):
        return str(self.id)

    @property
    def is_digital(self):
        qs = self.products.all()  # every product
        new_qs = qs.filter(is_digital=False)  # every product that is not digial
        if new_qs.exists():
            return False
        return True


def m2m_changed_cart_receiver(sender, instance, action, *args, **kwargs):
    if action == 'post_add' or action == 'post_remove' or action == 'post_clear':
        products = instance.products.all()
        total = 0
        for x in products:
            total += x.price
        if instance.subtotal != total:
            instance.subtotal = total
            instance.save()

m2m_changed.connect(m2m_changed_cart_receiver, sender=Cart.products.through)




def pre_save_cart_receiver(sender, instance, *args, **kwargs):
    if instance.subtotal > 0:
        instance.total = Decimal(instance.subtotal)  # 8% tax
    else:
        instance.total = 0.00

pre_save.connect(pre_save_cart_receiver, sender=Cart)


# class Order(models.Model):
#     ref_code = models.CharField(max_length=15)
#     owner = models.ForeignKey(Profile, on_delete=models.SET_NULL, null=True)
#     is_ordered = models.BooleanField(default=False)
#     items = models.ManyToManyField(Cart)
#     date_ordered = models.DateTimeField(auto_now=True)
#
#     def get_cart_items(self):
#         return self.items.all()
#
#     def get_cart_total(self):
#         return sum([item.product.price for item in self.items.all()])
#
#     def __str__(self):
#         return '{0} - {1}'.format(self.owner, self.ref_code)
#
#
# class Transaction(models.Model):
#     profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
#     token = models.CharField(max_length=120)
#     order_id = models.CharField(max_length=120)
#     amount = models.DecimalField(max_digits=100, decimal_places=2)
#     success = models.BooleanField(default=True)
#     timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
#
#     def __str__(self):
#         return self.order_id
#
#     class Meta:
#         ordering = ['-timestamp']