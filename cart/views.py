from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Cart
from P2P_Renting.models import Post
from django.http import JsonResponse
from django.shortcuts import render, redirect


@login_required
def cart_main(request):
    print(request.POST)
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    print(cart_obj)
    return render(request, "cart/home.html", {"cart": cart_obj})

@login_required
def cart_update(request):
    print(request.POST)
    added = False
    product_id = request.POST.get('product_id')
    # product_id = 1
    if product_id is not None:
        try:
            product_obj = Post.objects.get(id=product_id)
        except Post.DoesNotExist:
            print("Show message to user, product is gone?")
            return redirect("cart")
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        if product_obj in cart_obj.products.all():
            cart_obj.products.remove(product_obj)
            added = False
            print("Product Removed")
        else:
            cart_obj.products.add(product_obj)  # cart_obj.products.add(product_id)
            added = True
            print("Product Added")
        request.session['cart_items'] = cart_obj.products.count()
        # return redirect(product_obj.get_absolute_url())
        # if request.is_ajax():  # Asynchronous JavaScript And XML / JSON
        #     print("Ajax request")
        #     json_data = {
        #         "added": added,
        #         "removed": not added,
        #         "cartItemCount": cart_obj.products.count()
        #     }
        #     return JsonResponse(json_data, status=200)  # HttpResponse
            # return JsonResponse({"message": "Error 400"}, status=400) # Django Rest Framework
    page = request.POST.get('page')
    if page == "home_page":
        return redirect("P2P_Renting-Home")
    elif page == "cart_page":
        return redirect("cart")


