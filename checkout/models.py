from django.db import models
from django.db.models.signals import pre_save
from django.contrib.auth.models import User
from cart.models import Cart

from .utils import unique_transaction_id_generator

#.........Choices..........
PaymentStatus = (
    ("created", "Created"),
    ("paid", "Paid"),
    ("cancelled", "Cancelled"),
    ("failed", "Failed"),
    ("error", "Error"),
)
#..........................

class TransactionManager(models.Manager):
    def new(self, request):
        print(request)
        new_obj = False
        tran_obj = None
        # cart_id = request.session.get("cart_id", None)
        user_instance = request.user
        cart_id = request.POST.get("cart")
        print(cart_id)
        cart_obj = Cart.objects.filter(id = cart_id)
        cart_obj_first = cart_obj.first()
        print("User: " + str(user_instance))
        print("Cart Instance: " + str(cart_obj_first))
        if request.user.is_authenticated:
            if user_instance is not None and cart_obj_first is not None:
                tran_obj = self.model.objects.create(user=user_instance, cart=cart_obj_first, subtotal = cart_obj_first.subtotal, total=cart_obj_first.total)
                new_obj = True
            request.session['tran_pk_id'] = tran_obj.id
        return tran_obj, new_obj


class Transaction(models.Model):
    transaction_id = models.CharField(max_length=120, blank = False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null = False)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, null=False, unique= False)
    customer_ip_address = models.GenericIPAddressField(blank=True, null=True)
    ## Use this or divide the properties among all attributes and include list of items
    currency = models.CharField(max_length=10, default = "rupees")
    subtotal = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    delivery = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    discount = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    tax = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    total = models.DecimalField(max_digits=9, decimal_places=2, default='0.0')
    ## <Payment Gateways are to be set here> ............
    token = models.CharField(max_length=36, blank=True, default='')
    ## < end > of all payment gateways ... Paytm, Paypal, UPI, Amex, Sodexo, Credit and Debit Cards
    payment_status = models.CharField(max_length=10, choices=PaymentStatus, default="created")
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    objects = TransactionManager()

    def __str__(self):
        return str(self.id)

def pre_save_create_transaction_id(sender, instance, *args, **kargs):
    if not instance.transaction_id:
        instance.transaction_id = unique_transaction_id_generator(instance)

pre_save.connect(pre_save_create_transaction_id, sender=Transaction)