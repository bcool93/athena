# Generated by Django 2.1.5 on 2019-10-03 06:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cart', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('transaction_id', models.CharField(max_length=120)),
                ('customer_ip_address', models.GenericIPAddressField(blank=True, null=True)),
                ('currency', models.CharField(default='rupees', max_length=10)),
                ('subtotal', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('delivery', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('discount', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('tax', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('total', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('token', models.CharField(blank=True, default='', max_length=36)),
                ('payment_status', models.CharField(choices=[('created', 'Created'), ('paid', 'Paid'), ('cancelled', 'Cancelled'), ('failed', 'Failed'), ('error', 'Error')], default='created', max_length=10)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('cart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart.Cart')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
