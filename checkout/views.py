from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from .models import Transaction
from .utils import generate_checksum, verify_checksum

MERCHANT_ID = 'OVDZey59769378739038'
MERCHANT_SECRET_KEY = 'v1CvS_bF1#5Zdu24'

@login_required
def checkout_main(request):
    print(request.POST)
    # cart_obj, new_obj = Cart.objects.new_or_get(request)
    transaction_obj, new_obj  = Transaction.objects.new(request)
    print(transaction_obj)


    return render(request, "checkout/checkout_final.html", {"transaction": transaction_obj})


def call_paythm_handle(request):
    if request.method == "POST":
        print("Request in Call Paytm Handle: " + str(request.POST))
        transaction_id = request.POST.get('transaction_id')
        transaction_obj = Transaction.objects.get(id=transaction_id)
        data_dict = {
            'MID': str(MERCHANT_ID),
            'ORDER_ID': str(transaction_obj.id),
            'TXN_AMOUNT': str(transaction_obj.total),
            'CUST_ID': transaction_obj.user.email,
            'INDUSTRY_TYPE_ID': 'Retail',
            'WEBSITE': 'athenawebstaging',
            'CHANNEL_ID': 'WEB',
            'CALLBACK_URL':'http://localhost:8012/paymentrequest/',
        }
        data_dict['CHECKSUMHASH'] = generate_checksum(data_dict, MERCHANT_SECRET_KEY)
        print("Paytm Data Dict: " + str(data_dict))
        return render(request, "checkout/paytm.html", {'param_dict': data_dict})


@csrf_exempt
def paymentrequest(request):
    # paytm will send you post request here
    form = request.POST
    response_dict = {}
    for i in form.keys():
        response_dict[i] = form[i]
        if i == 'CHECKSUMHASH':
            checksum = form[i]

    verify = verify_checksum(response_dict, MERCHANT_SECRET_KEY, checksum)
    if verify:
        if response_dict['RESPCODE'] == '01':
            print('order successful')
        else:
            print('order was not successful because' + response_dict['RESPMSG'])
    print("Final Response Dict: " + str(response_dict))
    return render(request, 'checkout/paymentstatus.html', {'response': response_dict})



